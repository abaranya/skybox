from flask import Flask, request
from app_service import AppService
import json

app = Flask(__name__)
# app_service = AppService('')

@app.route('/')
def home():
    return "Hello World"

@app.route('/about/')
def about():
    return "<h2>This is a Flask web application.</h2>"

"""
@app.route('/api/tasks')
def tasks():
    return app_service.get_tasks()

@app.route('/api/task', methods=['POST'])
def create_task():
    request_data = request.get_json()
    task = request_data['task']
    return app_service.create_task(task)

@app.route('/api/task', methods=['PUT'])
def update_task():
    request_data = request.get_json()
    return app_service.update_task(request_data['task'])

@app.route('/api/task/<int:id>', methods=['DELETE'])
def delete_task(id):
    print(id)
    return app_service.delete_task(id)
"""

if __name__ == "__main__":
    app.run(debug=True)
