# syntax=docker/dockerfile:1

FROM python:3.11-slim-buster

WORKDIR /skyapp


COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

#until we can map
#COPY . /skyapp

ENV FLASK_APP=skyapp.py
ENV FLASK_DEBUG=1

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
