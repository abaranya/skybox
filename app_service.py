import json


class AppService(object):
    """AppService for ."""

    tasks = [
        {
            'id': 1,
            'name': "task1",
            "description": "This is task 1"
        },
        {
            "id": 2,
            "name": "task2",
            "description": "This is task 2"
        },
        {
            "id": 3,
            "name": "task3",
            "description": "This is task 3"
        }
    ]

    def __init__(self, arg):
        # uper(AppService_init__())
        self.arg = arg
        self.tasks_json = json.dumps(self.tasks)

    def get_tasks(self):
        self.tasks_json = json.dumps(self.tasks)
        return self.tasks_json

    def create_task(self,task):
        self.tasks.append(task)
        self.tasks_json = json.dumps(self.tasks)
        return self.tasks_json

    def update_task(self, request_task):
        for task in self.tasks:
            if task["id"] == request_task['id']:
                task.update(request_task)
                self.tasks_json = json.dumps(self.tasks)
                return self.tasks_json
        return json.dumps({'message': 'task id not found'})

    def delete_task(self, request_task_id):
        for task in self.tasks:
            if task["id"] == request_task_id:
                self.tasks.remove(task)
                self.tasks_json = json.dumps(self.tasks)
                return self.tasks_json
        return json.dumps({'message': 'task id not found'})
